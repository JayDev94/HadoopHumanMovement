package project;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class RunClass extends Configured implements Tool {

	//// IN ECLIPSE TESTING
	/*
	 * String inputTest =
	 * "/home/one/Documents/FinalProject/hadoop/TESTING/input/testinputCopy.txt";
	 * String outputTest =
	 * "/home/one/Documents/FinalProject/hadoop/TESTING/output"; String holdTest
	 * = "/home/one/Documents/FinalProject/hadoop/TESTING/hold";
	 */

	public static void main(String[] args) throws Exception {
		int res = ToolRunner.run(new Configuration(), new RunClass(), args);
		System.exit(res);
	}

	@Override
	public int run(String[] args) throws Exception {

		Path hold = new Path("/HoldOutputFile"); 

		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "FinalProject");
		job.setJarByClass(RunClass.class);

		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, hold);
		/*
		 * FileInputFormat.addInputPath(job, new Path(inputTest));
		 * FileOutputFormat.setOutputPath(job, new Path(holdTest));
		 */

		// job.setOutputFormatClass(SequenceFileOutputFormat.class);

		boolean completion = job.waitForCompletion(true);

		// If first job is successful, run second
		if (completion) {
			Job job2 = Job.getInstance(conf, "FinalProjectPart2");

			// job2.setInputFormatClass(KeyValueTextInputFormat.class);

			job2.setJarByClass(RunClass.class);

			job2.setMapperClass(SecondMapper.class);
			job2.setReducerClass(SecondReducer.class);

			job2.setOutputKeyClass(Text.class);
			job2.setOutputValueClass(IntWritable.class);

			job2.setMapOutputKeyClass(Text.class);
			job2.setMapOutputValueClass(Text.class);

			FileInputFormat.addInputPath(job2, hold);
			FileOutputFormat.setOutputPath(job2, new Path(args[1]));
			/*
			 * FileInputFormat.addInputPath(job2, new Path(holdTest));
			 * FileOutputFormat.setOutputPath(job2, new Path(outputTest));
			 */

			if (job2.waitForCompletion(true)) {
				// Delete the temp hold folder
				FileSystem fs = FileSystem.get(conf);
				fs.delete(hold, true);
			}

			return job2.waitForCompletion(true) ? 0 : 1;

		} 
		return job.waitForCompletion(true) ? 0 : 1;

	}

}

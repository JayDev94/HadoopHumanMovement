package project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Values {

	private String disaster = "";
	private String coordinates = "";
	private Long miliDate = 0L;

	private String strDate = "";

	public Values() {

	}

	public Values(Long miliDate, String coordinates) {

		this.miliDate = miliDate;
		this.coordinates = coordinates;
	}

	public Long getMiliDate() {
		return miliDate;
	}

	public String getDates() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(miliDate);
		;
		Date date = cal.getTime();

		return date.toString();
	}

	public String getCoordinates() {
		return coordinates;
	}


	public void setDates(Long miliDate) {
		this.miliDate = miliDate;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public void setDisaster(String disaster) {
		this.disaster = disaster;
	}

}

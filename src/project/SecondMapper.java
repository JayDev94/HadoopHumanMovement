package project;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class SecondMapper extends Mapper<Object, Text, Text, Text> {

	public void map(Object key, Text val, Context context) throws IOException, InterruptedException {

		String line = val.toString();

		String[] three = line.split("\t"); /// ID DIS VALUES (v0,v1,v2,v3)

		// String[]valueTokens = three[2].split(",");
		// //<TOTALDIST>,<DURATION>,<AVGDISTPERDAY>,<LOCATIONCHANGES>

		context.write(new Text(three[1]), new Text(three[0] + "," + three[2]));

	}
}
package project;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapper extends Mapper<Object, Text, Text, Text> {

	private Text indication = new Text();

	private final IntWritable count = new IntWritable(1);

	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public void map(Object key, Text val, Context context) throws IOException, InterruptedException {

		if (val.toString().length() != 0) {
			String[] token = val.toString().split(",");
			String disaster = token[0];
			String userId = token[1];
			String latLong = token[2] + "," + token[3];
			String date = token[4];

			try {
				Calendar calendar = Calendar.getInstance();
				Date dateMili = dateFormat.parse(date);
				calendar.setTime(dateMili);
				Long miliDate = calendar.getTimeInMillis();
				String strMiliDate = "" + miliDate;

				context.write(new Text(userId +"\t"+disaster), new Text(strMiliDate + "\t" + latLong));

			} catch (ParseException e) {
				e.printStackTrace();
			}

		}

	}

}

package project;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducer extends Reducer<Text, Text, Text, Text> {

	private IntWritable count = new IntWritable();
	Text textDistance = null;

	Text word = null;

	Text textVal = null;
	String[] firstLatLong = null;

	String firstLat = "";
	String firstLon = "";

	String prevLat = "";
	String prevLon = "";

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public void reduce(Text key, Iterable<Text> val, Context context) throws IOException, InterruptedException {

		int i = 0;
		int x = 0;

		ArrayList<String> dateList = new ArrayList<String>();
		ArrayList<String> coordinateList = new ArrayList<String>();

		List<String> allDateList = new ArrayList<String>();
		List<Values> allValuesList = new ArrayList<Values>();

		double totalDistance = 0;
		String strTotalDistance = "";

		String duration = "";

		String currentCoordinateString = "";

		String disaster = "";

		// if (key.toString().equals("0")) {
		while (val.iterator().hasNext()) {
			word = val.iterator().next();

			//// VALUE SORTING
			//// -----------------------------------------------------------
			String[] valueTokens = word.toString().split("\t");
			Values values = new Values(Long.parseLong(valueTokens[0]), valueTokens[1]);
			allValuesList.add(values);

			if (val.iterator().hasNext() == false) {
				Collections.sort(allValuesList, new Comparator<Values>() {
					@Override
					public int compare(Values values1, Values values2) {
						// Compare the mili value of dates
						return values1.getMiliDate().compareTo(values2.getMiliDate());
					}
				});

				// Distance calculations =============================
				for (int z = 0; z < allValuesList.size(); z++) {
					// System.out.println("Coordinates: " +
					// allValuesList.get(z).getCoordinates());
					// Acquire first coordinate
					if (z == 0) {
						coordinateList.add(allValuesList.get(z).getCoordinates());

						continue;
					}
					
					// Acquire first distance
					if (z == 1) {
						String[] prevCord = coordinateList.get(0).toString().split(",");
						String[] currentCord = allValuesList.get(z).getCoordinates().split(",");

						double distance = CalculateDistance(Double.parseDouble(prevCord[0]),
								Double.parseDouble(currentCord[0]), Double.parseDouble(prevCord[1]),
								Double.parseDouble(currentCord[1]));
						totalDistance = totalDistance + distance;
						strTotalDistance = "" + totalDistance;

						coordinateList.remove(0); // Remove the first word
													// from
													// the
													// //
						coordinateList.add(allValuesList.get(z).getCoordinates()); // Add
																					// the
						// current
						// word
						// to replace 

						continue;
					} else if (z > 1) {
						String[] prevCord = coordinateList.get(0).split(",");
						String[] currentCord = allValuesList.get(z).getCoordinates().split(",");

						double distance = CalculateDistance(Double.parseDouble(prevCord[0]),
								Double.parseDouble(currentCord[0]), Double.parseDouble(prevCord[1]),
								Double.parseDouble(currentCord[1]));

						// System.out.println("DistanceCalculatd: " +
						// distance);
						totalDistance = totalDistance + distance;
						totalDistance = Math.round(totalDistance * 1000000.0) / 1000000.0;

						coordinateList.remove(0);
						coordinateList.add(allValuesList.get(z).getCoordinates());

						strTotalDistance = totalDistance + "";

						/*
						 * System.out.println("KEY: " + key +
						 * "  TotalDistance: " + strTotalDistance +
						 * "   Coordinates: " +
						 * allValuesList.get(z).getCoordinates());
						 */
					}

				}

				// User activity duration ================
				
				duration = getUserDuration(df, allValuesList.get(0).getMiliDate(),
						allValuesList.get(allValuesList.size() - 1).getMiliDate());
			}
			//// VALUE SORTING
			//// -----------------------------------------------------------

			i++;
		}

		// }
		// System.out.println("StrTotalDistance is: " + strTotalDistance + "
		// Duration: " + duration);
		String valueOutput = "";
		if (strTotalDistance.isEmpty() || duration.isEmpty() || strTotalDistance.equals("0")) {
			strTotalDistance = "0.0";
			valueOutput = strTotalDistance + "," + duration
					+ "," + 0 + "," + allValuesList.size();
		} else {
			double distanceByDays = Double.parseDouble(strTotalDistance) / Double.parseDouble(duration);
			distanceByDays = Math.round(distanceByDays * 100.0) / 100.0;
			valueOutput = strTotalDistance + "," + duration
					+ "," + distanceByDays + ","
					+ allValuesList.size();
		}
		context.write(key, new Text(valueOutput));
	}	

	// ------------------------------------------------------------------------------------//

	// Law of cosines algorithm ----Return miles
	public double CalculateDistance(double lat1, double lat2, double lon1, double lon2) {
		double r = 6371008;
		double d;
		
		double latRad1, latRad2, lon12;
		double Rad = (Math.PI / 180);
		latRad1 = Math.toRadians(lat1);
		latRad2 = Math.toRadians(lat2);
		lon12 = Math.toRadians(lon2 - lon1);
		d = Math.acos(Math.sin(latRad1) * Math.sin(latRad2) + Math.cos(latRad1) * Math.cos(latRad2) * Math.cos(lon12))
				* r;

		// Return distance in m rounded to the nearest 1000th
		// d = Math.round(d * 100000) / 100000.0;

		double conversion = 1609.34;
		d = d / conversion;
		d = Math.round(d * 1000000.0) / 1000000.0;

		return d;
	}

	// Test time length calculation
	public String getUserDuration(DateFormat d, long a, long b) {

		long diff = Math.abs(b - a);

		long daysDiff = TimeUnit.MILLISECONDS.toDays(diff);

		String strDays = "" + daysDiff;

		// System.out.println("TEST --- DurationFunction -- : " + daysDiff);

		return strDays;

	}

}

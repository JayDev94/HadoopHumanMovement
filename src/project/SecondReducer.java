package project;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class SecondReducer extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Iterable<Text> val, Context context) throws IOException, InterruptedException {
		int i = 1;
		Long totalDisasterIdDistance = 0l;
		Long totalLocationChanges = 0l;
		double durationSum = 0;
		
		
		Text textLine = null;
		String outputValues = "";
		

		while (val.iterator().hasNext()) {
			i += 1;   

			textLine = val.iterator().next();

			// ID, TotalDistance, Duration, AvgDistance, LocationChanges
			String[] splitValues = textLine.toString().split(",");
			Long totalDistance = (long)Math.round(Double.parseDouble(splitValues[1]));
			int duration = Integer.parseInt(splitValues[2]);
			Long  avgDistance = (long)Math.round(Double.parseDouble(splitValues[3]));
			Long locationChanges = Long.parseLong(splitValues[4]);

			totalLocationChanges = totalLocationChanges + locationChanges;
			totalDisasterIdDistance = totalDisasterIdDistance + totalDistance;
			//totalAvgDistance = totalAvgDistance + avgDistance;
			durationSum = durationSum + duration;

		
			if (val.iterator().hasNext() == false) {
				/*avgDisasterDistance = (long) Math.round(totalDisasterIdDistance / i);
				avgLocationChanges = (long) Math.round(totalLocationChanges / i);
				avgDistancePerDay = (long) Math.round(totalAvgDistance / i);
				System.out.println("avgDisasterDistance: " + avgDisasterDistance +"   avgLocationChanges: " + avgLocationChanges +"   avgDistancePerDay: " + avgDistancePerDay);
				System.out.println("Total ID Count: " + i);
				double roundedAvgDistDuration = avgDisasterDistance;  
				double roundedAvgLocationChanges  = avgLocationChanges;    
				double roundedAvgDistancePerDay = avgDistancePerDay;      
				avgDistanceDuration = round(avgDistanceDuration);
				avgLocationChanges = round(avgLocationChanges);
				avgDistancePerDay = round(avgDistancePerDay);*/

				/*outputValues = totalDisasterIdDistance + "\t" + totalLocationChanges + "\t" + roundedAvgDistDuration + "\t"
						+ roundedAvgLocationChanges + "\t" + totalAvgDistance + "\t" + roundedAvgDistancePerDay;*/
				
				
				double avgActivityPerUser = durationSum / i;
				
				outputValues = totalDisasterIdDistance + "\t" + totalLocationChanges + "\t" + durationSum + "\t" + avgActivityPerUser;
				
			}

		}

		context.write(key, new Text(outputValues));

	}

	/*
	public double round100(double x) {
		double y = Math.round(x * 100.0) / 100.0;
		return y;
	}
	public double round10(double x){
		double y = Math.round(x * 100.0) / 100.0;
		return y;
	}*/

}
